package com.mindvalley.mvchallengeun.library.httpClient;

import android.content.Context;

import java.io.File;
import java.io.IOException;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by William on 28/9/2016.
 *
 * okhttpclient
 */

public class OHC {
    private OkHttpClient client;
    public OnResponse onResponse;

    //cache settings
    public File cacheDirectory;
    private int cacheSize = 1024 * 1024 * 10;//10 mb default cache size
    private static int cacheDuration = 60 * 60 * 24 * 7;//a week

    private static final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public Response intercept(Interceptor.Chain chain) throws IOException {
            Response originalResponse = chain.proceed(chain.request());
            return originalResponse.newBuilder()
                    .header("Cache-Control", String.format("max-age=%d, only-if-cached, max-stale=%d", cacheDuration, 0))
                    .build();
        }
    };

    public OHC(Context context) {
        cacheDirectory = context.getCacheDir();
    }

    public interface OnResponse {
        void success(Response response);
        void failure();
    }

    public void async(String url, final OnResponse onResponse) {
        Request request = new Request.Builder()
                .url(url)
                .build();

        client = new OkHttpClient.Builder().cache(new Cache(cacheDirectory, cacheSize))
                                           .addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
                                           .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();

                onResponse.failure();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                onResponse.success(response);
            }
        });
    }

    public OkHttpClient getClient() {
        return client;
    }

    public void setClient(OkHttpClient client) {
        this.client = client;
    }
}
