package com.mindvalley.mvchallengeun.library;

import android.content.Context;

import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyAsyncDownloader;
import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyAsyncTask;
import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyImageAsyncTask;
import com.mindvalley.mvchallengeun.library.httpClient.MindvalleySyncDownloader;

/**
 * Mindvalley downloader library
 *
 * Created by William on 1/10/2016.
 */

public class MindvalleyD {
    /**
     * Download files synchronously
     *
     * @param context
     * @param url
     * @param onResponse
     * @return
     */
    public static MindvalleySyncDownloader downloadSynchronously(Context context, String url, MindvalleyAsyncTask.OnResponse onResponse) {
        final MindvalleySyncDownloader returnMe = new MindvalleySyncDownloader(context);

        returnMe.download(url, onResponse);

        return returnMe;
    }

    /**
     * Download files asynchronously
     *
     * @param context
     * @param url
     * @param onResponse
     * @return
     */
    public static MindvalleyAsyncDownloader downloadAsynchronously(Context context, String url, MindvalleyAsyncTask.OnResponse onResponse) {
        final MindvalleyAsyncDownloader returnMe = new MindvalleyAsyncDownloader(context);

        returnMe.download(url, onResponse);

        return returnMe;
    }

    /**
     * Download images asychronously
     *
     * @param context
     * @param url
     * @param width
     * @param height
     * @param onResponse
     * @return
     */
    public static MindvalleyAsyncDownloader downloadImageAsynchronously(Context context, String url, int width, int height, MindvalleyImageAsyncTask.OnResponse onResponse) {
        final MindvalleyAsyncDownloader returnMe = new MindvalleyAsyncDownloader(context);

        returnMe.download(url, width, height, onResponse);

        return returnMe;
    }
}
