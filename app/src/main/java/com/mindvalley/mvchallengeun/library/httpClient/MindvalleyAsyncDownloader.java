package com.mindvalley.mvchallengeun.library.httpClient;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by William on 28/9/2016.
 *
 * progress sample: https://github.com/square/okhttp/blob/master/samples/guide/src/main/java/okhttp3/recipes/Progress.java * not working :/
 *
 * using asyncTask+OkHttp
 */

public class MindvalleyAsyncDownloader {
    //cache settings
    private Context context;
    public static int cacheDuration = 60 * 60 * 24 * 7;//a week
    private MindvalleyImageAsyncTask mindvalleyImageAsyncTask;
    private MindvalleyAsyncTask mindvalleyAsyncTask;

    //Intercepts and overwrite's response cache header
    private static final Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Response originalResponse = chain.proceed(chain.request());

            return originalResponse.newBuilder()
                    .header("Cache-Control", String.format("max-age=%d, only-if-cached, max-stale=%d", cacheDuration, 0))
                    .build();
        }
    };

    private static final Interceptor LOGGING_INTERCEPTOR = new Interceptor() {
        @Override public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            long t1 = System.nanoTime();
            Log.d("logger", String.format("Sending request %s on %s%n%s", request.url(), chain.connection(), request.headers()));
            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            Log.d("logger", String.format("Received response for %s in %.1fms%n%s", response.request().url(), (t2 - t1) / 1e6d, response.headers()));

            return response;
        }
    };

    //Constructor
    public MindvalleyAsyncDownloader(Context context) {
        this.context = context;
    }

    //Asynchronous response
    public void download(String url, int width, int height, final MindvalleyImageAsyncTask.OnResponse onResponse) {
        OkHttpClient passMe = new OkHttpClient()
                                .newBuilder()
                                .cache(MindvalleyAsyncTask.createHttpClientCache(context))
                                //.addNetworkInterceptor(LOGGING_INTERCEPTOR)
                                .addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
                                .build();

        mindvalleyImageAsyncTask = new MindvalleyImageAsyncTask(url, passMe, onResponse, MindvalleyImageAsyncTask.getCacheFolder(context), width, height);
        mindvalleyImageAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void download(String url, final MindvalleyAsyncTask.OnResponse onResponse) {
        OkHttpClient passMe = new OkHttpClient()
                .newBuilder()
                .cache(MindvalleyAsyncTask.createHttpClientCache(context))
                //.addNetworkInterceptor(LOGGING_INTERCEPTOR)
                .addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
                .build();

        mindvalleyAsyncTask = new MindvalleyAsyncTask(url, passMe, onResponse, MindvalleyAsyncTask.getCacheFolder(context));
        mindvalleyAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void cancel() {
        if(mindvalleyImageAsyncTask !=  null) { mindvalleyImageAsyncTask.cancel(true); }
        if(mindvalleyAsyncTask !=  null) { mindvalleyAsyncTask.cancel(true); }
    }
}
