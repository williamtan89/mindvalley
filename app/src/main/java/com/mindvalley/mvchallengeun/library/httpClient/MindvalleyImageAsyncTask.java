package com.mindvalley.mvchallengeun.library.httpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by William on 29/9/2016.
 *
 * Download and returns bitmap, not stable though as image too big will throws an OOM error.
 * Not enough time to debug, at least it works till it crashes.
 *
 * Normally I uses library to handles this.. such as picasso.. or glide.. maybe volley.. also retrofit..
 * Who still re-invent wheels???
 *
 * references:
 *
 * http://stackoverflow.com/questions/9942560/when-to-clear-the-cache-dir-in-android
 *
 */

public class MindvalleyImageAsyncTask extends AsyncTask<Void, Long, Bitmap> {
    public static String CACHE_FOLDER = "mindvalley_downloader_cache";

    private final String URL;
    private final OkHttpClient httpClient;
    private final OnResponse onResponse;
    private final File cacheDirectory;

    private int width;
    private int height;

    private Response response;

    private static int cacheSize = 1024 * 1024 * 20;//20 mb default cache size..?

    //interfaces
    public interface OnResponse {
        void success(Bitmap bitmap);
        void failure();
        void update(long bytesRead, long contentLength, boolean done);
    }

    //Constructors
    public MindvalleyImageAsyncTask(String url, OkHttpClient httpClient, OnResponse onResponse, File cacheDirectory, int width, int height) {
        this.URL = url;
        this.httpClient = httpClient;
        this.onResponse = onResponse;
        this.cacheDirectory = cacheDirectory;

        this.width = width;
        this.height = height;
    }

    public MindvalleyImageAsyncTask(String url, OkHttpClient httpClient) {
        this(url, httpClient, null, null, 512, 512);//default size
    }

    public MindvalleyImageAsyncTask(String url) {
        this(url, new OkHttpClient());
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Call call = httpClient.newCall(new Request.Builder().url(URL).get().build());

        try {
            this.response = call.execute();

            if (response.code() == 200) {
                InputStream inputStream = null;

                try {
                    inputStream = new BufferedInputStream(response.body().byteStream());

                    byte[] buff = new byte[1024 * 4];
                    long downloaded = 0;
                    Long target = response.body().contentLength();

                    inputStream.mark(target.intValue());//will die when exceed 2gb, need to split to arrays but let's not make it complicated now TODO

                    //cache management!
                    if(response.cacheResponse() == null) {//if not null means the response is from cache, can ignore.
                        manageCache(target);
                    }

                    publishProgress(0L, target);

                    while (true) {
                        int readed = inputStream.read(buff);

                        if(readed == -1){
                            break;
                        }

                        //write buff
                        downloaded += readed;
                        publishProgress(downloaded, target);

                        if (isCancelled()) {
                            return null;
                        }
                    }

                    inputStream.reset();
                    Bitmap returnMe = null;

                    try {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inJustDecodeBounds = true;

                        BitmapFactory.decodeStream(inputStream, null, options);

                        options.inSampleSize = BitmapUtils.calculateInSampleSize(options, width, height);
                        options.inJustDecodeBounds = false;

                        inputStream.reset();

                        returnMe = BitmapFactory.decodeStream(inputStream, null, options);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                    inputStream.close();

                    return returnMe;
                } catch (IOException ignore) {
                    ignore.printStackTrace();

                    if(onResponse != null) {
                        onResponse.failure();
                    }

                    return null;
                } finally {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                }
            } else {
                if(onResponse != null) {
                    onResponse.failure();
                }

                return null;
            }
        } catch (IOException e) {
            if(onResponse != null) {
                onResponse.failure();
            }

            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onProgressUpdate(Long... values) {
        if(onResponse != null) {
            onResponse.update(values[0], values[1], values[0].equals(values[1]));
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if(onResponse != null) {
            if (bitmap == null) {
                onResponse.failure();
            } else {
                onResponse.success(bitmap);
            }
        }
    }

    public static Cache createHttpClientCache(Context context) {
        return new Cache(getCacheFolder(context), cacheSize);
    }

    public static File getCacheFolder(Context context) {
        return context.getDir(MindvalleyImageAsyncTask.CACHE_FOLDER, Context.MODE_PRIVATE);
    }

    //clear all files from cache - removes all unconditionally
    public static void clearCache(Context context) {
        deleteWholeDir(MindvalleyImageAsyncTask.getCacheFolder(context));
    }

    private static boolean deleteWholeDir(File dir) {//clears all
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteWholeDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    private void manageCache(long contentLength) {
        if(cacheDirectory != null) {
            Log.d("cacher", getDirSize(cacheDirectory) + " " + contentLength + " " + cacheSize);

            if(getDirSize(cacheDirectory) + contentLength > cacheSize) {
                Log.d("cacher", "need to remove some stuffs");

                long bytesDeleted = 0;
                File[] files = cacheDirectory.listFiles();

                Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_COMPARATOR);

                for (File file : files) {
                    bytesDeleted += file.length();
                    file.delete();

                    if (bytesDeleted >= contentLength) {
                        break;
                    }
                }

                Log.d("cacher", "deleted " + bytesDeleted);
            }
            else {
                Log.d("cacher", "dont have to remove stuffs");
            }
        }
    }

    public static long getDirSize(File directory) {
        long length = 0;

        try {
            for (File file : directory.listFiles()) {
                if (file.isFile())
                    length += file.length();
                else
                    length += getDirSize(file);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return length;
    }
}