package com.mindvalley.mvchallengeun.example.asynchronous;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;

import com.mindvalley.mvchallengeun.library.MindvalleyD;
import com.mindvalley.mvchallengeun.R;
import com.mindvalley.mvchallengeun.databinding.TestTwoLayoutBinding;

import java.util.ArrayList;
import java.util.Random;

import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyAsyncDownloader;
import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyAsyncTask;
import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyImageAsyncTask;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import com.mindvalley.mvchallengeun.example.model.UnsplashImage;

/**
 * Created by William on 28/9/2016.
 */

public class TestTwoAdapter extends RecyclerView.Adapter<TestTwoAdapter.FileDownloadView> implements RealmChangeListener {
    private ArrayList<MindvalleyAsyncDownloader> asyncDownloaders = new ArrayList<>();
    private RealmResults<UnsplashImage> unsplashImageRealmResults;
    private Context context;
    private int FADE_DURATION = 1000;

    public static class FileDownloadView extends RecyclerView.ViewHolder {
        private TestTwoLayoutBinding binding;
        private boolean dead = false;

        public FileDownloadView(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public TestTwoLayoutBinding getBinding() {
            return binding;
        }
    }

    public TestTwoAdapter(Context context, RealmResults<UnsplashImage> unsplashImageRealmResults) {
        this.context = context;
        this.unsplashImageRealmResults = unsplashImageRealmResults;
        this.unsplashImageRealmResults.addChangeListener(this);
    }

    @Override
    public FileDownloadView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_two_layout, parent, false);
        FileDownloadView holder = new FileDownloadView(v);

        return holder;
    }

    private String[] randomUrl = new String[]{
            "https://static.pexels.com/photos/27117/pexels-photo-27117.jpg",
            "https://static.pexels.com/photos/27116/pexels-photo-27116.jpg",
            "https://static.pexels.com/photos/27115/pexels-photo-27115.jpg",
            "https://static.pexels.com/photos/27114/pexels-photo-27114.jpg",
            "https://static.pexels.com/photos/27113/pexels-photo-27113.jpg",
            "https://static.pexels.com/photos/27112/pexels-photo-27112.jpg",
            "https://static.pexels.com/photos/27111/pexels-photo-27111.jpg",
            "https://static.pexels.com/photos/27110/pexels-photo-27110.jpg",
            "https://static.pexels.com/photos/27105/pexels-photo-27105.jpg",
            "https://static.pexels.com/photos/27104/pexels-photo-27104.jpg",
            "https://static.pexels.com/photos/27103/pexels-photo-27103.jpg",
            "https://static.pexels.com/photos/27102/pexels-photo-27102.jpg",
            "https://static.pexels.com/photos/27101/pexels-photo-27101.jpg"};

    @Override
    public void onBindViewHolder(final FileDownloadView holder, final int position) {
        final UnsplashImage unsplashImage = unsplashImageRealmResults.get(position);

        final String url = randomUrl[new Random().nextInt(randomUrl.length)];

        holder.getBinding().tvLineOne.setText(url);
        holder.getBinding().tvProgress.setText("0%");

        holder.getBinding().ivImage.post(new Runnable() {
            @Override
            public void run() {
                final MindvalleyAsyncDownloader asyncDownloader = MindvalleyD.downloadImageAsynchronously(context,
                    url,
                    holder.getBinding().ivImage.getWidth(),
                    holder.getBinding().ivImage.getHeight(),
                    new MindvalleyImageAsyncTask.OnResponse() {
                        @Override
                        public void success(final Bitmap bitmap) {
                            holder.dead = true;

                            //keep track of current cache size
                            long currentCacheDirSize = MindvalleyAsyncTask.getDirSize(MindvalleyAsyncTask.getCacheFolder(context));
                            float convertToMb = currentCacheDirSize / (1024f * 1024f);
                            ((LibraryTestTwoActivity) context).binding.tvCacheFolderSize.setText(String.format("Cache dir size: %.2f MB", convertToMb));

                            holder.binding.ivImage.setImageBitmap(bitmap);
                        }

                        @Override
                        public void failure() {
                            ((LibraryTestTwoActivity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    holder.dead = true;
                                    holder.getBinding().tvProgress.setText("FAILED");
                                }
                            });
                        }

                        @Override
                        public void update(final long bytesRead, final long contentLength, final boolean done) {
                            ((LibraryTestTwoActivity) context).runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //prevents canceled from being overwrite, i could had use a more sophisticated method like boolean isCanceled... oh mannnn TODO
                                    if(holder.getBinding().tvProgress.getText().toString().indexOf("CANCELED") < 0) {
                                        holder.getBinding().tvProgress.setText(((int) (bytesRead * 100 / contentLength)) + "%");
                                    }

                                    if(done) {
                                        holder.dead = true;
                                        holder.getBinding().tvProgress.setText("DONE");
                                    }

                                    if(holder.getBinding().tvLineTwo.getText().toString().isEmpty()) {
                                        holder.getBinding().tvLineTwo.setText(String.format("File size: %.2f MB", (contentLength / (1024f * 1024f))));
                                    }
                                }
                            });
                        }
                    });

                asyncDownloaders.add(asyncDownloader);

                //click progress to cancel download
                holder.getBinding().tvProgress.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(!holder.dead) {
                            asyncDownloader.cancel();
                            holder.getBinding().tvProgress.setText("CANCELED");
                        }
                    }
                });
            }
        });

        setFadeAnimation(holder.itemView);
    }

    //fade item animation on first showing
    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return unsplashImageRealmResults.size();
    }

    @Override
    public void onChange(Object element) {
        notifyItemInserted(unsplashImageRealmResults.size()-1);
    }

    public void stopDownloads() {
        for(MindvalleyAsyncDownloader asyncDownloader: asyncDownloaders) {
            asyncDownloader.cancel();
        }

        asyncDownloaders.clear();//removes all
    }
}
