package com.mindvalley.mvchallengeun.example.model;

import io.realm.RealmObject;

/**
 * Created by William on 28/9/2016.
 */

public class UnsplashProfileImageUrl extends RealmObject {
    private String small;
    private String medium;
    private String large;

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }
}
