package com.mindvalley.mvchallengeun.example;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;

import com.mindvalley.mvchallengeun.R;
import com.mindvalley.mvchallengeun.databinding.MasonryLayoutBinding;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import com.mindvalley.mvchallengeun.example.model.UnsplashImage;

/**
 * Created by William on 28/9/2016.
 */

public class MasonryAdapter extends RecyclerView.Adapter<MasonryAdapter.MasonryView> implements RealmChangeListener {
    private RealmResults<UnsplashImage> unsplashImageRealmResults;
    private Context context;
    private int FADE_DURATION = 1000;

    @Override
    public void onChange(Object element) {
        notifyItemInserted(unsplashImageRealmResults.size()-1);
    }

    public static class MasonryView extends RecyclerView.ViewHolder {
        private MasonryLayoutBinding binding;

        public MasonryView(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public MasonryLayoutBinding getBinding() {
            return binding;
        }
    }

    public MasonryAdapter(Context context, RealmResults<UnsplashImage> unsplashImageRealmResults) {
        this.context = context;
        this.unsplashImageRealmResults = unsplashImageRealmResults;
        this.unsplashImageRealmResults.addChangeListener(this);
    }

    @Override
    public MasonryView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.masonry_layout, parent, false);
        MasonryView holder = new MasonryView(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MasonryView holder, final int position) {
        holder.itemView.post(new Runnable() {
            @Override
            public void run() {
                if(position < unsplashImageRealmResults.size()) {//for reasons unknown sometimes it will crash the app upon reinstall, after crash the app can be opened normally. TODO
                    final UnsplashImage unsplashImage = unsplashImageRealmResults.get(position);

                    ((RelativeLayout.LayoutParams) holder.getBinding().ivImage.getLayoutParams()).width = holder.itemView.getWidth();

                    //get proportional height given width
                    int imageWidth = unsplashImage.getWidth();
                    int imageHeight = unsplashImage.getHeight();

                    if (holder.itemView.getWidth() > imageWidth) {//if image's width is lesser than cell width
                        imageHeight = (int) Math.ceil(imageHeight * (holder.itemView.getWidth() / imageWidth));
                    } else {//if image's width is greater than cell width
                        imageHeight = (imageHeight / (imageWidth / holder.itemView.getWidth()));
                    }

                    ((RelativeLayout.LayoutParams) holder.getBinding().ivImage.getLayoutParams()).height = imageHeight;
                    holder.getBinding().tvTitle.setText(unsplashImage.getUser().getName());
                    holder.getBinding().tvFav.setText(unsplashImage.getLikes() + "");

                    if (unsplashImage.getUrls().getRegular() == null) {//random image shouldn't be cache
                        //picasso hates vector drawable :(

                        Picasso.with(context).load("https://unsplash.it/" + holder.getBinding().ivImage.getLayoutParams().width + "/" + holder.getBinding().ivImage.getLayoutParams().height + "/?random")
                                .memoryPolicy(MemoryPolicy.NO_CACHE)
                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                .placeholder(R.drawable.progress_animation)
                                .into(holder.getBinding().ivImage);
                    } else {
                        Picasso.with(context).load(unsplashImage.getUrls().getRegular())
                                .placeholder(R.drawable.progress_animation)
                                .into(holder.getBinding().ivImage);
                    }

                    Picasso.with(context).load(unsplashImage.getUser().getProfile_image().getMedium()).into(holder.getBinding().rivUserProfile);
                }
            }
        });

        setFadeAnimation(holder.itemView);
    }

    //fade item animation on first showing
    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return unsplashImageRealmResults.size();
    }
}
