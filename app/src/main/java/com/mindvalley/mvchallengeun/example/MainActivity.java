package com.mindvalley.mvchallengeun.example;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.mindvalley.mvchallengeun.R;
import com.mindvalley.mvchallengeun.databinding.ActivityMainBinding;
import com.mindvalley.mvchallengeun.example.asynchronous.LibraryTestTwoActivity;
import com.mindvalley.mvchallengeun.example.synchronous.LibraryTestOneActivity;

import java.io.IOException;
import java.util.Date;

import com.mindvalley.mvchallengeun.library.httpClient.OHC;
import io.realm.Realm;
import com.mindvalley.mvchallengeun.example.model.UnsplashImage;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    private String sampleDataUrl = "http://pastebin.com/raw/wgkJgazE";

    private ActivityMainBinding binding;
    private Realm realm;

    private static String LOG_TAG = "MAIN_ACTIVITY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setSupportActionBar((Toolbar) findViewById(R.id.tb_main));
        getSupportActionBar().setTitle("Main Activity");

        // Initialize Realm. Should only be done once when the application starts.
        Realm.init(this);

        realm = Realm.getDefaultInstance();

        //load sample api data
        loadSampleApiData(true);
    }

    public void loadSampleApiData(final boolean init) {
        binding.srlMain.setRefreshing(true);

        //Uses OkHttpClient to get json data
        OHC ohc = new OHC(MainActivity.this);
        ohc.async(sampleDataUrl, new OHC.OnResponse() {
            @Override
            public void success(Response response) {
                try {
                    final String responseString = response.body().string();
                    //Log.d("sample data url", responseString);

                    Log.d(LOG_TAG, ""+response.cacheResponse());
                    Log.d(LOG_TAG, ""+response.networkResponse());

                    // Create the Realm instance as this is actually a different thread
                    Realm newRealm = Realm.getDefaultInstance();
                    newRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            //clear off previous data, if any
                            realm.deleteAll();

                            //populate realm
                            realm.createOrUpdateAllFromJson(UnsplashImage.class, responseString);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if(init) {
                                        afterInit();
                                    }
                                    else {
                                        binding.srlMain.setRefreshing(false);
                                    }
                                }
                            });
                        }
                    });
                }
                catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }

            @Override
            public void failure() {
                Log.d("sample data url", "failed :/");
            }
        });
    }

    public void afterInit() {
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        layoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);

        binding.rvMain.setLayoutManager(layoutManager);
        binding.rvMain.setItemAnimator(null);
        binding.rvMain.setItemViewCacheSize(1000);//for test
        binding.rvMain.addItemDecoration(new MasonryItemDecoration(8));

        MasonryAdapter adapter = new MasonryAdapter(MainActivity.this, realm.where(UnsplashImage.class).findAll());
        binding.rvMain.setAdapter(adapter);

        binding.fabAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.createOrUpdateAllFromJson(UnsplashImage.class, genenerateSampleIndividualJson());
                    }
                });


                //set appbarlayout expanded to false or else smooth scroll position will not show accurately
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        binding.ablMain.setExpanded(false, true);
                        binding.rvMain.smoothScrollToPosition(binding.rvMain.getAdapter().getItemCount());
                    }
                }, 200);
            }
        });

        binding.srlMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                loadSampleApiData(false);
            }
        });

        binding.srlMain.setRefreshing(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Close the Realm instance.
        realm.close();
    }

    //must return string in jsonArray format
    public static String genenerateSampleIndividualJson() {
        String stringId = Long.toString(new Date().getTime(), 32);
        return "[{\"id\":\"" + stringId + "\",\"created_at\":\"2016-05-29T15:42:02-04:00\",\"width\":2448,\"height\":1836,\"color\":\"#060607\",\"likes\":12,\"liked_by_user\":false,\"user\":{\"id\":\"OevW4fja2No\",\"username\":\"nicholaskampouris\",\"name\":\"Nicholas Kampouris\",\"profile_image\":{\"small\":\"https://images.unsplash.com/profile-1464495186405-68089dcd96c3?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=faces\\u0026fit=crop\\u0026h=32\\u0026w=32\\u0026s=63f1d805cffccb834cf839c719d91702\",\"medium\":\"https://images.unsplash.com/profile-1464495186405-68089dcd96c3?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=faces\\u0026fit=crop\\u0026h=64\\u0026w=64\\u0026s=ef631d113179b3137f911a05fea56d23\",\"large\":\"https://images.unsplash.com/profile-1464495186405-68089dcd96c3?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=faces\\u0026fit=crop\\u0026h=128\\u0026w=128\\u0026s=622a88097cf6661f84cd8942d851d9a2\"},\"links\":{\"self\":\"https://api.unsplash.com/users/nicholaskampouris\",\"html\":\"http://unsplash.com/@nicholaskampouris\",\"photos\":\"https://api.unsplash.com/users/nicholaskampouris/photos\",\"likes\":\"https://api.unsplash.com/users/nicholaskampouris/likes\"}},\"current_user_collections\":[],\"urls\":{\"raw\":\"https://images.unsplash.com/photo-1464550883968-cec281c19761\",\"full\":\"https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026s=4b142941bfd18159e2e4d166abcd0705\",\"small\":\"https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026w=400\\u0026fit=max\\u0026s=d5682032c546a3520465f2965cde1cec\",\"thumb\":\"https://images.unsplash.com/photo-1464550883968-cec281c19761?ixlib=rb-0.3.5\\u0026q=80\\u0026fm=jpg\\u0026crop=entropy\\u0026w=200\\u0026fit=max\\u0026s=9fba74be19d78b1aa2495c0200b9fbce\"},\"categories\":[{\"id\":4,\"title\":\"Nature\",\"photo_count\":46148,\"links\":{\"self\":\"https://api.unsplash.com/categories/4\",\"photos\":\"https://api.unsplash.com/categories/4/photos\"}},{\"id\":6,\"title\":\"People\",\"photo_count\":15513,\"links\":{\"self\":\"https://api.unsplash.com/categories/6\",\"photos\":\"https://api.unsplash.com/categories/6/photos\"}}],\"links\":{\"self\":\"https://api.unsplash.com/photos/4kQA1aQK8-Y\",\"html\":\"http://unsplash.com/photos/4kQA1aQK8-Y\",\"download\":\"http://unsplash.com/photos/4kQA1aQK8-Y/download\"}}]";
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mi_info:
                // Use the Builder class for convenient dialog construction
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("Main Activity Info");

                //yes, it's a one liner super long string ¯\_(ツ)_/¯ please don't penalise me on this (._. )a; please dont turn on softwrap >.>
                String makeMeBeautiful = "Techniques applied:\n\n1. Using third-party libraries:\n\nPicasso - loading images, placeholders, cache\n\nOkHttpClient - load json file *normally I would use Retrofit as my main http client but for the sake of demo I'll just try OkHttpClient*\n\nRealm - ORM, used for managing provided sample data\n\nRoundedImageView - profile rounded image view\n\n2. Material design UI elements (FAB, Toolbar-hide-on-scroll)\n\n3. Swipe Refresh Layout / Pull to refresh\n\n4. Does applying SpannableString in alertdialog count for something?\n\nPsst, remember to try Demo One and Demo Two on the Three-Dots-Icon thingy";
                SpannableStringBuilder prettyString = new SpannableStringBuilder(makeMeBeautiful);

                prettyString.setSpan(new StyleSpan(Typeface.BOLD), makeMeBeautiful.indexOf("Picasso"), makeMeBeautiful.indexOf("Picasso") + "Picasso".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                prettyString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), makeMeBeautiful.indexOf("Picasso"), makeMeBeautiful.indexOf("Picasso") + "Picasso".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                prettyString.setSpan(new StyleSpan(Typeface.BOLD), makeMeBeautiful.indexOf("OkHttpClient"), makeMeBeautiful.indexOf("OkHttpClient") + "OkHttpClient".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                prettyString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), makeMeBeautiful.indexOf("OkHttpClient"), makeMeBeautiful.indexOf("OkHttpClient") + "OkHttpClient".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                prettyString.setSpan(new StyleSpan(Typeface.BOLD), makeMeBeautiful.indexOf("RoundedImageView"), makeMeBeautiful.indexOf("RoundedImageView") + "RoundedImageView".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                prettyString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), makeMeBeautiful.indexOf("RoundedImageView"), makeMeBeautiful.indexOf("RoundedImageView") + "RoundedImageView".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                prettyString.setSpan(new StyleSpan(Typeface.BOLD), makeMeBeautiful.indexOf("Realm"), makeMeBeautiful.indexOf("Realm") + "Realm".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
                prettyString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)), makeMeBeautiful.indexOf("Realm"), makeMeBeautiful.indexOf("Realm") + "Realm".length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);

                builder.setMessage(prettyString);
                builder.setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //do what? no idea lol leave it blanks it will close itself
                    }
                });

                // Create the AlertDialog object and return it
                builder.create().show();

                return true;
            case R.id.mi_test_one: {
                Intent newIntent = new Intent(MainActivity.this, LibraryTestOneActivity.class);
                startActivity(newIntent);

                return true;
            }
            case R.id.mi_test_two: {
                Intent newIntent = new Intent(MainActivity.this, LibraryTestTwoActivity.class);
                startActivity(newIntent);

                return true;
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
