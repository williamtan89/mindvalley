package com.mindvalley.mvchallengeun.example.asynchronous;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.mindvalley.mvchallengeun.R;
import com.mindvalley.mvchallengeun.databinding.ActivityLibraryTestTwoBinding;
import com.mindvalley.mvchallengeun.example.HeaderDecoration;
import com.mindvalley.mvchallengeun.example.MainActivity;

import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyAsyncTask;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import com.mindvalley.mvchallengeun.example.model.UnsplashImage;

/**
 * Created by William on 28/9/2016.
 */

public class LibraryTestTwoActivity extends AppCompatActivity {
    public ActivityLibraryTestTwoBinding binding;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_library_test_two);

        //realm unique to this activity only
        final RealmConfiguration config = new RealmConfiguration.Builder()
                .name("testTwo")
                .build();

        Realm.deleteRealm(config);//fresh start!
        realm = Realm.getInstance(config);

        setSupportActionBar((Toolbar) findViewById(R.id.tb_test_two));
        getSupportActionBar().setTitle("Test Two");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        RelativeLayout rvTestTwoHeader = (RelativeLayout) getLayoutInflater().inflate(R.layout.rv_test_two_header, null);

        binding.rvTestTwo.setLayoutManager(new LinearLayoutManager(LibraryTestTwoActivity.this));
        binding.rvTestTwo.setItemAnimator(null);
        binding.rvTestTwo.setItemViewCacheSize(1000);//for test
        binding.rvTestTwo.addItemDecoration(new HeaderDecoration(rvTestTwoHeader, false, 0, 10f, 1));

        TestTwoAdapter adapter = new TestTwoAdapter(LibraryTestTwoActivity.this, realm.where(UnsplashImage.class).findAll());
        binding.rvTestTwo.setAdapter(adapter);

        binding.srlTestTwo.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                binding.srlTestTwo.setRefreshing(false);
            }
        });

        binding.btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.createOrUpdateAllFromJson(UnsplashImage.class, MainActivity.genenerateSampleIndividualJson());

                        //set appbarlayout expanded to false or else smooth scroll position will not show accurately
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.ablTestTwo.setExpanded(false, true);
                                binding.rvTestTwo.smoothScrollToPosition(binding.rvTestTwo.getAdapter().getItemCount());
                            }
                        }, 150);
                    }
                });
            }
        });

        binding.srlTestTwo.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MindvalleyAsyncTask.clearCache(LibraryTestTwoActivity.this);//clear cache?

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //stop any remaining download
                        ((TestTwoAdapter) binding.rvTestTwo.getAdapter()).stopDownloads();

                        realm.deleteAll();//wipe off - clean slate!
                        binding.srlTestTwo.setRefreshing(false);
                    }
                });

                binding.tvCacheFolderSize.setText("Cache dir size: 0.00 MB");
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Close the Realm instance.
        realm.close();
    }
}
