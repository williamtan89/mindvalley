package com.mindvalley.mvchallengeun.example.model;

import io.realm.RealmObject;

/**
 * Created by William on 28/9/2016.
 */

public class UnsplashImageUrl extends RealmObject {
    private String raw;
    private String full;
    private String regular;
    private String small;
    private String thumb;

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String getRegular() {
        return regular;
    }

    public void setRegular(String regular) {
        this.regular = regular;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }
}
