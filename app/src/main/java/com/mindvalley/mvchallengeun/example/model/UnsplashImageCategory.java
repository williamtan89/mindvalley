package com.mindvalley.mvchallengeun.example.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by William on 28/9/2016.
 */

public class UnsplashImageCategory extends RealmObject {
    @PrimaryKey private long id;
    private String title;
    private int photo_count;
    private UnsplashLink links;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPhoto_count() {
        return photo_count;
    }

    public void setPhoto_count(int photo_count) {
        this.photo_count = photo_count;
    }

    public UnsplashLink getLinks() {
        return links;
    }

    public void setLinks(UnsplashLink links) {
        this.links = links;
    }
}
