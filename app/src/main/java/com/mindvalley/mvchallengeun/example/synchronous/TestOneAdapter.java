package com.mindvalley.mvchallengeun.example.synchronous;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;

import com.mindvalley.mvchallengeun.library.MindvalleyD;
import com.mindvalley.mvchallengeun.R;
import com.mindvalley.mvchallengeun.databinding.TestOneLayoutBinding;

import java.util.ArrayList;

import com.mindvalley.mvchallengeun.library.httpClient.MindvalleySyncDownloader;
import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyAsyncTask;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import com.mindvalley.mvchallengeun.example.model.UnsplashImage;
import okhttp3.Response;

/**
 * Created by William on 28/9/2016.
 */

public class TestOneAdapter extends RecyclerView.Adapter<TestOneAdapter.FileDownloadView> implements RealmChangeListener {
    private ArrayList<MindvalleySyncDownloader> dhcv2AL = new ArrayList<>();
    private RealmResults<UnsplashImage> unsplashImageRealmResults;
    private Context context;
    private int FADE_DURATION = 1000;

    public static class FileDownloadView extends RecyclerView.ViewHolder {
        private TestOneLayoutBinding binding;

        public FileDownloadView(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }

        public TestOneLayoutBinding getBinding() {
            return binding;
        }
    }

    public TestOneAdapter(Context context, RealmResults<UnsplashImage> unsplashImageRealmResults) {
        this.context = context;
        this.unsplashImageRealmResults = unsplashImageRealmResults;
        this.unsplashImageRealmResults.addChangeListener(this);
    }

    @Override
    public FileDownloadView onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_one_layout, parent, false);
        FileDownloadView holder = new FileDownloadView(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(final FileDownloadView holder, final int position) {
        final UnsplashImage unsplashImage = unsplashImageRealmResults.get(position);

        holder.getBinding().tvLineOne.setText("10 MB File");
        holder.getBinding().tvLineTwo.setText(unsplashImage.getUser().getName());
        holder.getBinding().tvProgress.setText("0%");

        //download file
        final MindvalleySyncDownloader dtcv2 = MindvalleyD.downloadSynchronously(context, "http://web4host.net/10MB.zip", new MindvalleyAsyncTask.OnResponse() {
            @Override
            public void success(Response response) {
                //
            }

            @Override
            public void failure() {
                //
            }

            @Override
            public void update(long bytesRead, long contentLength, boolean done) {
                //prevents canceled from being overwrite, i could had use a more sophisticated method... oh mannnn TODO
                if(holder.getBinding().tvProgress.getText().toString().indexOf("CANCELED") < 0) {
                    holder.getBinding().tvProgress.setText(((int) (bytesRead * 100 / contentLength)) + "%");
                }

                if(done) {
                    holder.getBinding().tvProgress.setText("DONE");
                }
            }
        });

        //click progress to cancel download
        holder.getBinding().tvProgress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dtcv2.cancel();
                holder.getBinding().tvProgress.setText("CANCELED");
            }
        });

        setFadeAnimation(holder.itemView);
    }

    //fade item animation on first showing
    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    @Override
    public int getItemCount() {
        return unsplashImageRealmResults.size();
    }

    @Override
    public void onChange(Object element) {
        notifyItemInserted(unsplashImageRealmResults.size()-1);
    }

    public void stopDownloads() {
        for(MindvalleySyncDownloader dhcv2: dhcv2AL) {
            dhcv2.cancel();
        }

        dhcv2AL.clear();//removes all
    }
}
