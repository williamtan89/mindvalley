package com.mindvalley.mvchallengeun.example.synchronous;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.mindvalley.mvchallengeun.R;
import com.mindvalley.mvchallengeun.databinding.ActivityLibraryTestOneBinding;
import com.mindvalley.mvchallengeun.example.HeaderDecoration;
import com.mindvalley.mvchallengeun.example.MainActivity;

import com.mindvalley.mvchallengeun.library.httpClient.MindvalleyAsyncTask;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import com.mindvalley.mvchallengeun.example.model.UnsplashImage;

/**
 * Created by William on 28/9/2016.
 */

public class LibraryTestOneActivity extends AppCompatActivity {
    private ActivityLibraryTestOneBinding binding;
    private Realm realm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_library_test_one);

        //realm unique to this activity only
        final RealmConfiguration config = new RealmConfiguration.Builder()
                .name("testOne")
                .build();

        Realm.deleteRealm(config);//fresh start!
        realm = Realm.getInstance(config);

        setSupportActionBar((Toolbar) findViewById(R.id.tb_test_one));
        getSupportActionBar().setTitle("Test One");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        RelativeLayout rvTestOneHeader = (RelativeLayout) getLayoutInflater().inflate(R.layout.rv_test_one_header, null);

        binding.rvTestOne.setLayoutManager(new LinearLayoutManager(LibraryTestOneActivity.this));
        binding.rvTestOne.setItemAnimator(null);
        binding.rvTestOne.setItemViewCacheSize(1000);//for test
        binding.rvTestOne.addItemDecoration(new HeaderDecoration(rvTestOneHeader, false, 0, 0, 1));

        TestOneAdapter adapter = new TestOneAdapter(LibraryTestOneActivity.this, realm.where(UnsplashImage.class).findAll());
        binding.rvTestOne.setAdapter(adapter);

        binding.srlTestOne.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                binding.srlTestOne.setRefreshing(false);
            }
        });

        binding.btnAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.createOrUpdateAllFromJson(UnsplashImage.class, MainActivity.genenerateSampleIndividualJson());

                        //set appbarlayout expanded to false or else smooth scroll position will not show accurately
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                binding.ablTestOne.setExpanded(false, true);
                                binding.rvTestOne.smoothScrollToPosition(binding.rvTestOne.getAdapter().getItemCount());
                            }
                        }, 150);
                    }
                });
            }
        });

        binding.srlTestOne.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MindvalleyAsyncTask.clearCache(LibraryTestOneActivity.this);//clear cache?

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        //stop any remaining download
                        ((TestOneAdapter) binding.rvTestOne.getAdapter()).stopDownloads();

                        realm.deleteAll();//wipe off - clean slate!
                        binding.srlTestOne.setRefreshing(false);
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Close the Realm instance.
        realm.close();
    }
}
