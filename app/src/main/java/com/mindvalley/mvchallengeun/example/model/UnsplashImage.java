package com.mindvalley.mvchallengeun.example.model;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by William on 28/9/2016.
 *
 * For demo purposes
 */

public class UnsplashImage extends RealmObject {
    @PrimaryKey private String id;
    private Date created_at;
    private int width;
    private int height;
    private String color;
    private int likes;
    private boolean liked_by_user;
    private UnsplashUser user;
    private RealmList<UnsplashUser> current_user_collection;
    private UnsplashImageUrl urls;
    private RealmList<UnsplashImageCategory> categories;
    private UnsplashLink links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public UnsplashUser getUser() {
        return user;
    }

    public void setUser(UnsplashUser user) {
        this.user = user;
    }

    public UnsplashImageUrl getUrls() {
        return urls;
    }

    public void setUrls(UnsplashImageUrl urls) {
        this.urls = urls;
    }

    public UnsplashLink getLinks() {
        return links;
    }

    public void setLinks(UnsplashLink links) {
        this.links = links;
    }

    public RealmList<UnsplashUser> getCurrent_user_collection() {
        return current_user_collection;
    }

    public void setCurrent_user_collection(RealmList<UnsplashUser> current_user_collection) {
        this.current_user_collection = current_user_collection;
    }

    public RealmList<UnsplashImageCategory> getCategories() {
        return categories;
    }

    public void setCategories(RealmList<UnsplashImageCategory> categories) {
        this.categories = categories;
    }
}
