package com.mindvalley.mvchallengeun.example.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by William on 28/9/2016.
 */

public class UnsplashUser extends RealmObject {
    @PrimaryKey private String id;
    private String username;
    private String name;
    private UnsplashProfileImageUrl profile_image;
    private UnsplashLink links;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UnsplashLink getLinks() {
        return links;
    }

    public void setLinks(UnsplashLink links) {
        this.links = links;
    }

    public UnsplashProfileImageUrl getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(UnsplashProfileImageUrl profile_image) {
        this.profile_image = profile_image;
    }
}
